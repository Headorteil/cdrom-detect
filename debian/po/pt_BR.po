# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Translation of Debian Installer templates to Brazilian Portuguese.
# This file is distributed under the same license as debian-installer.
#
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2008-2012.
# Adriano Rafael Gomes <adrianorg@debian.org>, 2010-2020.
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2001-2002.
#   Free Software Foundation, Inc., 2000
#   Juan Carlos Castro y Castro <jcastro@vialink.com.br>, 2000-2005.
#   Leonardo Ferreira Fontenelle <leonardof@gnome.org>, 2006-2009.
#   Lisiane Sztoltz <lisiane@conectiva.com.br>
#   Tobias Quathamer <toddy@debian.org>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2020-04-12 20:04-0300\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@debian.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "Load drivers from removable media?"
msgstr "Carregar drivers a partir de uma mídia removível?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr "Nenhum dispositivo para mídia de instalação foi detectado."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"Pode ser que você precise carregar drivers adicionais a partir de uma mídia "
"removível, como um disquete ou pendrive. Se você possui tal mídia disponível "
"agora, insira-a e continue. Caso contrário, será oferecida a opção de "
"selecionar manualmente alguns módulos."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "Detectando hardware para encontrar mídia de instalação"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "Manually select a module and device for installation media?"
msgstr ""
"Selecionar manualmente um módulo e dispositivo para mídia de instalação?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr ""
"Nenhum dispositivo para mídia de instalação (como uma unidade de CD-ROM) foi "
"detectado."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"Se a sua unidade de CD-ROM for uma antiga unidade Mitsumi ou outra unidade "
"de CD-ROM não-IDE ou não-SCSI, você deverá escolher qual módulo carregar e o "
"dispositivo a ser usado. Se você não sabe qual módulo e dispositivo são "
"necessários, procure por alguma documentação ou tente uma instalação via "
"rede."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid "Retry mounting installation media?"
msgstr "Tentar montar a mídia de instalação novamente?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Não foi possível montar sua mídia de instalação. Ao instalar a partir de um "
"CD-ROM, isto provavelmente significa que o disco não estava na unidade. Se "
"este for o caso, você pode inseri-lo na unidade e tentar novamente."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid "Module needed for accessing the installation media:"
msgstr "Módulo necessário para acessar a mídia de instalação:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"A detecção automática não encontrou nenhuma unidade para a mídia de "
"instalação. Ao instalar a partir de um CD-ROM quando você possui uma unidade "
"de CD-ROM incomum (uma que não seja nem IDE, nem SCSI), você pode tentar "
"carregar um módulo específico."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid "Device file for accessing the installation media:"
msgstr "Arquivo de dispositivo para acessar a mídia de instalação:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"Para acessar sua mídia de instalação (como o seu CD-ROM), por favor, informe "
"o arquivo de dispositivo que deverá ser usado. Unidades de CD-ROM fora dos "
"padrões usam arquivos de dispositivos fora dos padrões (como /dev/mcdx)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Você pode alternar para o shell no segundo terminal (ALT+F2) para checar os "
"dispositivos disponíveis em /dev com o comando \"ls /dev\". Você poderá "
"retornar a esta tela pressionando ALT+F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "Lendo mídia de instalação"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "Lendo ${DIR}..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid "Installation media detected"
msgstr "Mídia de instalação detectada"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"A auto detecção da mídia de instalação funcionou. Foi encontrada uma unidade "
"que contém '${cdname}'. A instalação continuará agora."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "Mídia UNetbootin detectada"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"Aparentemente, a sua mídia de instalação foi gerada usando o UNetbootin. O "
"UNetbootin é regularmente ligado a relatos feitos por usuários de problemas "
"irreproduzíveis ou difíceis; se você tiver problemas usando essa mídia de "
"instalação, por favor, tente a sua instalação novamente sem usar o "
"UNetbootin antes de reportar problemas."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"O guia de instalação contém mais informação sobre como criar uma mídia de "
"instalação USB diretamente sem o UNetbootin."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Incorrect installation media detected"
msgstr "Mídia de instalação incorreta detectada"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "The detected media cannot be used for installation."
msgstr "A mídia detectada não pode ser usada para instalação."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Please provide suitable media to continue with the installation."
msgstr "Por favor, forneça uma mídia adequada para continuar com a instalação."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Erro lendo arquivo 'Release'"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"A mídia de instalação não parece conter um arquivo 'Release' válido ou esse "
"arquivo não pode ser lido corretamente."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"Você pode tentar repetir a detecção de mídia mas, mesmo se isso funcionar na "
"segunda tentativa, você pode enfrentar problemas posteriormente na "
"instalação."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "Desmontando/ejetando mídia de instalação..."

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
msgid "Detect and mount installation media"
msgstr "Detectar e montar mídia de instalação"
