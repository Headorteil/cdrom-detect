# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of lo.po to Lao
# Lao translation of debian-installer.
# Copyright (C) 2006-2010 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Anousak Souphavanh <anousak@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: lo\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2012-04-25 09:05+0700\n"
"Last-Translator: Anousak Souphavanh <anousak@gmail.com>\n"
"Language-Team: Lao <lo@li.org>\n"
"Language: lo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
#, fuzzy
msgid "Load drivers from removable media?"
msgstr "ໂຫລດໄດເວີຈາກສື່ຖອດສຽບ"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
#, fuzzy
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"ເຈົ້າອາດຈຳເປັນຕ້ອງໂຫຼດໄດເວີຊີດີອອກເພີ່ມເຕີມຈາກສື່ຖອດສຽບ ເຊັ່ນ: ແຜ່ນດິດທີ່ເກັບໄດເວີ ຖ້າເຈົ້າມີສື່ດັ່ງກ່ວາ "
"ກາລຸນາໃຊ້ເຂົ້າມາແລ້ວດຳເນີນການຕໍ່ຫຼື ບໍ່ສະນັ້ນ ເຈົ້າກໍ່ມີທາງເລືອກໃຫ້ເລືອກໂມດູນຊີດີດ້ວຍຕົວເອງ."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
#, fuzzy
msgid "Detecting hardware to find installation media"
msgstr "ກຳລັງກວດສອບຮາດແວເພື່ອຫາຮາດດິກ"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid "Manually select a module and device for installation media?"
msgstr "ຈະເລືອກໂມດູນ ແລະ ອຸປະກອນຊີດີອອກດ້ວຍຕົວເອງຫຼືບໍ່ ?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr "ກວດບໍ່ພົບໄດເວີຊີດີຮອມທີ່ຮູ້ຈັກ."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"ໄດເວີຊີດີຂອງເຈົ້າອາດເປັນໄດເວີ Mitsumi ເກົ່າ ຫຼື ເປັນໄດເວີທີ່ບໍ່ແມ່ນ IDE ຫຼື  SCSI ຊື່ງໃນກໍ່ລະນີ້ດັ່ງກ່ວາ "
"ເຈົ້າຄວນເລືອກໂມດູນທີ່ຈະເລືອກ ແລະ ອຸປະກອນທີ່ຈະໃຊ້ເອງ ຖ້າເຈົ້າບໍ່ຮູ້ວ່າຈະໃຊ້ໂມດູນຫຼືອຸປະກອນໃດ "
"ກາລຸນາກວດສອບເອກະສານຄູ່ມື ຫຼືລອງໃຊ້ວີທີ່ຕິດຕັ້ງຜ່ານເຄືອຄ່າຍແທນການຕິດຕັ້ງດ້ວຍຊີດີອອກ."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
#, fuzzy
msgid "Retry mounting installation media?"
msgstr "ລະຫັດຜ່ານສຳລັບການຕິດຕັ້ງຈາກໄລຍະໄກ:"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
#, fuzzy
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"ບໍ່ສາມາດເມົາແຜ່ນຊີດີອອກຕິດຕັ້ງໄດ້ ເປັນໄປໄດ້ວ່າເຈົ້າບໍ່ໄດ້ໃສ່ແຜ່ນໃນໄດເວີ ຊື່ງຖ້າໃຊ້ "
"ເຈົ້າສາມາດໃສ່ແຜ່ນແລ້ວລອງໃໝ່ໄດ້."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
#, fuzzy
msgid "Module needed for accessing the installation media:"
msgstr "ໂມດູນທີ່ຕ້ອງໃຊ້ເພື່ອຈະໃຊ້ງານຊີດີຮອມ:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
#, fuzzy
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"ການກວດສອບອຸປະກອນໂດຍອັດຕາໂນມັດຫາໄດຊີດີຮອມບໍ່ພົບສາມາດພາຍາຍາຍໂຫຼດໂມດູນເຈາະຈົງກໍ່ໄດ້ "
"ຖ້າໄດຊີດີຮອມຂອງເຈົ້າບໍ່ໃຊ້ໄດປົກກະຕິ (ກ່ວາຄື ບໍ່ແມ່ນທັງ IDE ແລະ SCSI)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
#, fuzzy
msgid "Device file for accessing the installation media:"
msgstr "ແຟ້ມອຸປະກອນສຳລັບໃຊ້ງານຊີດີຮອມ:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
#, fuzzy
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"ເພື່ອຈະໃຫ້ໃຊ້ງານໄດຊີດີຮອມໄດ້ ກາລຸນາປ້ອນຊື່ແຟ້ມອຸປະກອນສຳລັບໄດເວີທີ່ຈະໃຊ້ ໄດເວີຊີດີທີ່ບໍ່ໄດ້ມາດຕະຖານ "
"(ເຊັ່ນ /dev/mcdx)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"ຄຸນອາດສະຫຼັບໄປທີ່ໃນເທີ່ມີນັດທີ່ສອງ (ALT+F2) ເພື່ອກວດສອບອຸປະກອບທີ່ມີໃນ /devດ້ວຍຄຳສັ່ງ \"ls /dev\" "
"ໄດ້ ແລະເຈົ້າສາມາດກັບມາທີ່ໜ້າຈໍນີ້ໄດ້ ໂດຍກົດ ALT+F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
#, fuzzy
msgid "Scanning installation media"
msgstr "ຕິດຕັ້ງ SILO ບໍ່ສຳເລັດ"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "ກຳລັງສຳຫລວດຂໍ້ມູນໃນ${DIR}..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
#, fuzzy
msgid "Installation media detected"
msgstr "ບໍ່ພົບອີນເທີຣເຟຣດເຄືອຂ່າຍ"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
#, fuzzy
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"ກວດສອບຊີດີຮອມໂດຍອັດຕາໂນມັດໄດ້ສຳເລັດ ພົບໄດເວີຊີວີຮອມ ແລະ ມີແຜ່ນຊີດີ${cdname}"
"ຈະດຳເນີນການຕິດຕັ້ງຕໍ່ໄປ"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
#, fuzzy
msgid "UNetbootin media detected"
msgstr "ບໍ່ພົບອີນເທີຣເຟຣດເຄືອຂ່າຍ"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "Incorrect installation media detected"
msgstr "ບໍ່ພົບອີນເທີຣເຟຣດເຄືອຂ່າຍ"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "The detected media cannot be used for installation."
msgstr "ໃນໄດເວີຊີດີຮອມມີແຜ່ນຊີດີທີ່ບໍ່ສາມາດໃຊ້ຕິດຕັ້ງໄດ້."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "Please provide suitable media to continue with the installation."
msgstr "ກາລຸນາໃສ່ແຜ່ນຊີດີທີ່ຖູກຕ້ອງເພື່ອດຳເນີນການຕິດຕັ້ງຕໍ່"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "ເກີດຂໍ້ຜິດພາດຂະນະອ່ານແຟ້ມ Release"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
#, fuzzy
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr "ເບີ່ງໃນແຜ່ນຊີດີບໍ່ມີແຜ່ນ 'Release' ທີ່ໃຊ້ການ ຫຼືບໍ່ສາມາດອ່ານແຟ້ມດັ່ງກ່ວາໄດ້ຄົບຖ້ວນ."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
#, fuzzy
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"ເຈົ້າອາດຈະພະຍາຍາມກວດຫາຊີດີຮອມອີກເທື່ອໜື່ງ "
"ແຕ່ເຖີ່ງຈະກວດພົບອີກເປັນຄັ້ງທີ່ສອງເຈົ້າກໍ່ອາດຈະຍັງພໍກັບບັນຫາເກົ່າອີກ ໃນການຕິດຕັ້ງ."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr ""

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
#, fuzzy
msgid "Detect and mount installation media"
msgstr "ໃຊ້ຄຳສັ່ງ \"exit\" ເພື່ອກັບເຂົ້າສູ່ລາຍການຕິດຕັ້ງ."
