# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@debian.org>, 2004.
# Priti Patil <prithisd@gmail.com>, 2007.
# Sampada Nakhare, 2007.
# Sandeep Shedmake <sshedmak@redhat.com>, 2009, 2010.
# localuser <sampadanakhare@gmail.com>, 2015.
# Nayan Nakhare <nayannakhare@rediffmail.com>, 2018.
# Prachi Joshi <josprachi@yahoo.com>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2023-12-10 18:08+0000\n"
"Last-Translator: omwani <omwani03+gitlab@gmail.com>\n"
"Language-Team: CDAC_DI\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "Load drivers from removable media?"
msgstr "काढण्यायोग्य माध्यमांकडून ड्रायव्हर्स लोड करायचे?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr "स्थापना मीडियासाठी कोणतेही डिव्हाइस आढळले नाही."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"आपल्याला काढण्यायोग्य माध्यमांमधून अतिरिक्त ड्राइव्हर्स लोड करण्याची आवश्यकता असू शकते, जसे "
"की ड्राइव्हर फ्लॉपी किंवा यूएसबी स्टिक. आपल्याकडे आता हे उपलब्ध असल्यास, मीडिया टाका "
"आणि सुरू ठेवा. अन्यथा, आपल्याला काही मॉड्यूल व्यक्तिचलितपणे निवडण्याचा पर्याय दिला जाईल."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "इंस्टॉलेशन मिडिया शोधण्यासाठी हार्डवेअर शोधत आहे"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "Manually select a module and device for installation media?"
msgstr "इन्स्टॉलेशन मिडीयासाठी व्यक्तिचलितपणे मॉड्यूल व डिव्हाइस निवडायचे?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr ""
"स्थापना मीडियासाठी कोणतेही डिव्हाइस आढळले नाही (जसे की एक सीडी-रॉम डिव्हाइस)."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"जर आपली सीडी-रॉम ड्राइव्ह जुनी मित्सुमी किंवा इतर नॉन-आयडीई, एससीएसआय नॉन-सीडी-रोम "
"ड्राइव्ह असेल तर आपण कोणते मॉड्यूल लोड करावे आणि डिव्हाइस वापरायचे ते निवडावे. आपल्याला "
"कोणत्या मॉड्यूल आणि डिव्हाइसची आवश्यकता आहे हे माहित नसल्यास, काही दस्तऐवजीकरण पहा "
"किंवा नेटवर्क स्थापना करण्याचा प्रयत्न करा."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid "Retry mounting installation media?"
msgstr "स्थापना मीडिया पुन्हा माउंट करायचा प्रयत्न करायचा?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"आपला इन्स्टॉलेशन मीडिया माउंट करणे शक्य नाही. याचा अर्थ असा असू शकतो की,सीडी-रॉमवरून "
"स्थापित करताना, डिस्क ड्राइव्हमध्ये नव्हती. तसे असल्यास आपण ती टाकून पुन्हा प्रयत्न करा."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid "Module needed for accessing the installation media:"
msgstr "इंस्टॉलेशन मिडिया ऍक्सेस करण्यासाठी लागणारे मॉड्युल:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"इन्स्टॉलेशन मीडियासाठी स्वयंचलित डिटेक्शनला ड्राइव्ह सापडला नाही. CD-ROM वरून इन्स्टॉल "
"करताना आणि तुमच्याकडे असामान्य CD-ROM ड्राइव्ह आहे (जे IDE किंवा SCSI नाही), तुम्ही "
"विशिष्ट मॉड्यूल लोड करण्याचा प्रयत्न करू शकता."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid "Device file for accessing the installation media:"
msgstr "इन्स्टॉलेशन मीडियामध्ये प्रवेश करण्यासाठी डिव्हाइस फाइल:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"तुमच्‍या इन्‍स्‍टॉलेशन मिडीयामध्‍ये प्रवेश करण्‍यासाठी (जसे की तुमच्‍या CD-ROM), कृपया वापरण्‍याची "
"साधन फाइल एंटर करा. नॉन-स्टँडर्ड CD-ROM ड्राइव्हस् नॉन-स्टँडर्ड डिव्हाइस फाइल्स वापरतात "
"(जसे की /dev/mcdx)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"तुम्ही दुसर्‍या टर्मिनलवरील शेलवर (ALT+F2) जाऊन \"ls /dev\" वापरून /dev मधे उपलब्ध "
"असलेली उपकरणे तपासू शकता. तुम्ही ALT+F1 दाबून परत या पडद्यावर येऊ शकता."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "इंस्टॉलेशन माध्यम स्कॅन करीत आहे"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "${DIR} तपासली जात आहे..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid "Installation media detected"
msgstr "इन्स्टॉलेशन मीडिया आढळला"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"इन्स्टॉलेशन मीडियाचे ऑटोडिटेक्शन यशस्वी झाले. एक ड्राइव्ह सापडला आहे ज्यामध्ये '${cdname}' "
"आहे. स्थापना आता सुरू राहील."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "यूनेटबुटइन माध्यम सापडले"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"तुमचे अधिष्ठापना माध्यम यूनेटबुटइन वापरून निर्माण झाले आहे असे दिसते. यूनेटबुटइन नियमीतपणे "
"वापरकर्त्यांद्वारा कठीण वा पुनरुत्पादन-अशक्य समस्यांबाबत अहवालित झाले आहे; तुम्हाला "
"हेअधिष्ठापना माध्यम वापरताना समस्या आल्यास, कृपया समस्या अहवालित करण्याअगोदर यूनेटबुटइन "
"न वापरता पुन्हा तुमची अधिष्ठापना करण्याचा प्रयत्न करा."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"यूनेटबुटइन न वापरता थेट यूएसबी अधिष्ठापना माध्यम कसे बनवावे याची अधिक माहिती "
"अधिष्ठापना मार्गदर्शकात आहे."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Incorrect installation media detected"
msgstr "चुकीचे इंस्टॉलेशन मीडिया आढळले"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "The detected media cannot be used for installation."
msgstr "शोधलेले माध्यम स्थापनेसाठी वापरले जाऊ शकत नाही."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Please provide suitable media to continue with the installation."
msgstr "कृपया प्रतिष्ठापन सुरू ठेवण्यासाठी योग्य माध्यम प्रदान करा."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Release फाइल वाचताना त्रुटी"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"इंस्टॉलेशन मीडियामध्ये वैध 'Release' फाइल आहे असे वाटत नाही, किंवा ती फाइल योग्यरित्या "
"वाचली जाऊ शकली नाही."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"तुम्ही मीडिया डिटेक्शनची पुनरावृत्ती करण्याचा प्रयत्न करू शकता, परंतु ते दुसऱ्यांदा यशस्वी झाले "
"तरीही, तुम्हाला नंतर इंस्टॉलेशनमध्ये समस्या येऊ शकतात."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "इंस्टॉलेशन मीडिया अनमाउंट करीत आहे / बाहेर काढत आहे..."

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
msgid "Detect and mount installation media"
msgstr "इन्स्टॉलेशन मीडिया शोधा आणि माउंट करा"
