# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_is.po to Icelandic
# Icelandic messages for debian-installer.
# This file is distributed under the same license as debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
#
# Copyright (C) 2010 Free Software Foundation
# Sveinn í Felli <sv1@fellsnet.is>, 2018, 2019.
#
# Translations from iso-codes:
# Copyright (C) 2002,2003, 2010, 2011, 2012 Free Software Foundation, Inc.
# Translations from KDE:
# Þórarinn Rúnar Einarsson <thori@mindspring.com>
# zorglubb <debian-user-icelandic@lists.debian.org>, 2008.
# Sveinn í Felli <sveinki@nett.is>, 2010.
# Alastair McKinstry, <mckinstry@computer.org>, 2002.
# Sveinn í Felli <sveinki@nett.is>, 2010, 2011, 2012, 2013.
# Alastair McKinstry <mckinstry@computer.org>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: Icelandic (Debian Installer)\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2019-10-08 08:56+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "Load drivers from removable media?"
msgstr "Hlaða inn reklum af fjarlægjanlegum miðli?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr "Ekkert tæki með uppsetningarmiðli fannst."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"Hugsanlegt er að þú þurfir að hlaða inn aukareklum af fjarlægjanlegum "
"gagnamiðli á borð við disklingi eða USB-lykli. Ef þú átt slíkan gagnamiðil "
"skaltu tengja hann eða setja í drif núna og halda áfram. Annars verður þér "
"boðið að velja reklaeiningar handvirkt."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "Leita að vélbúnaði til að finna uppsetningardiska"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "Manually select a module and device for installation media?"
msgstr "Velja kerfiseiningu og tæki handvirkt fyrir uppsetningardisk?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr ""
"Ekkert tæki með uppsetningarmiðli fannst (eins og t.d. CD-ROM geisladrif)."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"Ef geisladrifið þitt er gamalt Mitsumi eða önnur gerð sem tengist hvorki "
"IDE- né SCSI-braut. Ef það er tilfellið ættirðu að velja rekilinn sem hlaða "
"þarf, og hvaða drif skal nota. Ef þú veist ekki hvaða rekil nota skal "
"ættirðu að leita að upplýsingum um það, eða reyna netuppsetningu."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid "Retry mounting installation media?"
msgstr "Reyna aftur að tengja uppsetningarmiðil í skráakerfið?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Ekki tókst að tengja gagnamiðilinn þinn. Þetta þýðir líklega að ef þú varst "
"að setja upp af CD-ROM geisladiski að diskurinn hafi ekki verið í drifinu. "
"Ef svo er geturðu sett hann í og reynt aftur."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid "Module needed for accessing the installation media:"
msgstr "Rekill sem þarf til að eiga samskipti við uppsetningardrif:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"Sjálfvirk leit að uppsetningardiski bar ekki árangur. Þú getur reynt að "
"hlaða inn ákveðnum rekli ef þú ert með CD-ROM geisladrif sem tengist hvorki "
"IDE né SCSI."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid "Device file for accessing the installation media:"
msgstr "Slóð að uppsetningardrifinu:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"Til þess að nota uppsetningarmiðilinn þinn (eins og t.d. CD-ROM geisladrif) "
"þarft þú að slá inn slóðina að því. Óstöðluð geisladrif koma inn á "
"óstöðluðum slóðum (eins og t.d. /dev/mcdx)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Þú getur notað skelina á seinni sýndarskjánum (ALT+F2) til þess að gá hvað "
"er í boði undir /dev með \"ls /dev\". Þú getur komist aftur á þennan skjá "
"með ALT+F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "Leita að uppsetningardiskum"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "Skoða ${DIR}..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid "Installation media detected"
msgstr "Uppsetningarmiðill fannst"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"Sjálfvirk leit að uppsetningarmiðlum bar árangur. Drif fannst sem inniheldur "
"'${cdname}'. Uppsetningarferlinu mun nú haldið áfram."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "UNetbootin-miðill fannst"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"Það lítur út fyrir að uppsetningarmiðillinn hafir verið útbúinn með "
"UNetbootin. UNetbootin kemur oft við sögu í erfiðum villum hjá notendum eða "
"vandamálum sem ekki er hægt að endurtaka; ef þú átt í vandamálum með þennan "
"uppsetningarmiðil, reyndu fyrst að útbúa hann án þess að nota UNetbootin "
"áður en þú tilkynnir um vandamál."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"Leiðarvísir uppsetningar inniheldur frekari upplýsingar um hvernig hægt sé "
"að útbúa USB-uppsetningarmiðla án þess að nota UNetbootin."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Incorrect installation media detected"
msgstr "Rangur uppsetningarmiðill fannst"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "The detected media cannot be used for installation."
msgstr "Gagnamiðillinn sem fannst er ekki er nothæfur í uppsetningu."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Please provide suitable media to continue with the installation."
msgstr "Settu inn hentugan gagnamiðil til þess að halda uppsetningu áfram."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Villa við lestur skrárinnar 'Release'"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"Uppsetningardiskurinn er ekki með gilda 'Release'-skrá, eða þá að ekki tókst "
"að lesa skrána rétt."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"Þú getur reynt að endurtaka leitina, en þó hún takist í seinna skiptið "
"gætirðu lent í vandræðum síðar í uppsetningarferlinu."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "Aftengi/hendi út uppsetningardiski..."

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
msgid "Detect and mount installation media"
msgstr "Finna og tengja uppsetningarmiðla"
