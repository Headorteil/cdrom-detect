# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_bs.po to Bosnian
# Bosnian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Safir Secerovic <sapphire@linux.org.ba>, 2006.
# Armin Besirovic <armin@linux.org.ba>, 2008.
# Amar Tufo <bhllinux@gmail.com>, 2022.
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2001,2002.
#   Free Software Foundation, Inc., 2001,2002,2003,2004
#   Safir Šećerović <sapphire@linux.org.ba>, 2004,2006.
#   Vedran Ljubovic <vljubovic@smartnet.ba>, 2001
#   (translations from drakfw).
#   Translations from KDE:
#   Nesiren Armin <bianchi@lugbih.org>, 2002
#   Vedran Ljubovic <vljubovic@smartnet.ba>, 2002
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_bs\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2022-04-28 20:08+0000\n"
"Last-Translator: Amar Tufo <bhllinux@gmail.com>\n"
"Language-Team: Bosnian <lokal@linux.org.ba>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
#, fuzzy
msgid "Load drivers from removable media?"
msgstr "Učitaj upravljačke programe s prenosivog medija"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
#, fuzzy
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"Možda biste trebali učitati dodatne CD-ROM upravljačke programe sa "
"prenosivog medija poput flopi diskete. Ako imate takvu disketu, ubacite je u "
"pogon i nastavite. U suprotnom, pružiti će Vam se prilika da ručno odaberete "
"CD-ROM module."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
#, fuzzy
msgid "Detecting hardware to find installation media"
msgstr "Detektiram hardware da bi pronašao hard diskove"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid "Manually select a module and device for installation media?"
msgstr "Ručno odabrati CD-ROM modul i uređaj?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr "Uobičajeni CD-ROM pogon nije detektovan."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"Možda imate stari Mitsumi ili drugi ne-IDE, ne-SCSI CD-ROM čitač. U tom "
"slučaju možete navesti koji modul treba učitati i koji uređaj koristiti. Ako "
"ne znate koji su modul i uređaj potrebni, potražite neku dokumentaciju ili "
"pokušajte mrežnu instalaciju umjesto instalacije sa CD-ROM-a."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
#, fuzzy
msgid "Retry mounting installation media?"
msgstr "Šifra za daljinsku instalaciju:"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
#, fuzzy
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Vaš instalacioni CD-ROM se ne može montirati. Ovo vjerovatno znači da CD-ROM "
"nije u pogonu. Ako je tako, može te ga ubaciti i ponovo pokušati."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
#, fuzzy
msgid "Module needed for accessing the installation media:"
msgstr "Modul potreban za pristup CD-ROM-u:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
#, fuzzy
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"Autodetekcijom nije pronađen nijedan CD-ROM pogon. Možete pokušati sa "
"učitavanjem nekog od modula ako imate neuobičajen CD-ROM pogon (koji nije ni "
"IDE niti SCSI)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
#, fuzzy
msgid "Device file for accessing the installation media:"
msgstr "Datoteka uređaja za pristup CD-ROM-u:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
#, fuzzy
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"Kako bi pristupili vašem CD-ROM pogonu molim unesite datoteku uređaja koja "
"se treba koristiti. Nestandardni CD-ROM pogoni koriste nestandardne datoteke "
"uređaja (poput /dev/mcdx)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Možete se prebaciti na shell na drugom terminalu (ALT+F2) da biste "
"provjerili dostupne uređaje u /dev pomoću naredbe \"ls /dev\". Na ovaj ekran "
"se vraćate sa ALT+F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
#, fuzzy
msgid "Scanning installation media"
msgstr "Instalacija SILO-a nije uspjela"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "Pretražujem ${DIR}..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
#, fuzzy
msgid "Installation media detected"
msgstr "Mrežni interfejsi nisu detektovani"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
#, fuzzy
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"Autodetekcija CD-ROM-a je uspješna. CD-ROM pogon je pronađen sa slijedećim "
"ubačenim CD-om : ${cdname}. Instalacija će se sada nastaviti."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
#, fuzzy
msgid "UNetbootin media detected"
msgstr "Mrežni interfejsi nisu detektovani"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "Incorrect installation media detected"
msgstr "Mrežni interfejsi nisu detektovani"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "The detected media cannot be used for installation."
msgstr "CD-ROM pogon sadrži CD koji se ne može koristiti za instalaciju."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "Please provide suitable media to continue with the installation."
msgstr "Molim ubacite odgovarajući CD da biste nastavili instalaciju."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Greška pri čitanju Release datoteke"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
#, fuzzy
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"Izgleda da CD-ROM ne sadrži ispravnu 'Release' datoteku, ili da se datoteka "
"ne može ispravno pročitati."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
#, fuzzy
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"Možete pokušati ponoviti detekciju CD-ROM-a, ali čak i ako uspije drugi put, "
"možete iskusiti probleme kasnije u instalaciji."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "Unmontovanje/izbacivanje instalacijskog medija. . ."

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
#, fuzzy
msgid "Detect and mount installation media"
msgstr ""
"Koristite se \"exit\" naredbom da bi ste se vratili u instalacioni meni."
